#  #!/usr/bin/python
# *********************************************************************
# * Copyright (C) 2016 Jacopo Nespolo <j.nespolo@gmail.com>           *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This file is part of codicefiscale
# 
# Codicefiscale is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
# 
# Codicefiscale is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# 
# You should have received a copy of the GNU General Public License along 
# with Paper.  If not, see <http://www.gnu.org/licenses/>


from codici_catastali import codici_catastali
from checksum import even, odd, remainder

vowels = "aeiouAEIOU"

months = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'H',
          7: 'L', 8: 'M', 9: 'P', 10: 'R', 11: 'S', 12: 'T'}

def is_vowel(letter):
    return letter in vowels

def get_consonants(string):
    return [x for x in string if not is_vowel(x)]

def get_vowels(string):
    return [x for x in string if is_vowel(x)]


def cf(name, surname, sex, date_of_birth, place_of_birth):
    consonants = get_consonants(surname)
    vowels = get_vowels(surname)
    SUR = consonants + vowels + ['X', 'X', 'X']
    SUR = "".join(SUR[:3]).upper()
    
    consonants = get_consonants(name)
    if len(consonants) >= 4:
        NAM = [consonants[0], consonants[2], consonants[3]]
    elif len(consonants) == 3:
        NAM = consonants
    else:
        vowels = get_vowels(name)
        NAM = consonants + vowels + ['X', 'X', 'X']
    NAM = "".join(NAM[:3]).upper()

    YEAR = str(date_of_birth[2] % 100)
    MONTH = months[date_of_birth[1]]
    if sex.upper() == "M":
        DAY = str(date_of_birth[0])
    else:
        DAY = str(date_of_birth[0] + 40)
    if len(DAY) == 1:
        DAY = '0' + DAY

    POB = codici_catastali[place_of_birth]

    CF = "".join([SUR, NAM, YEAR, MONTH, DAY, POB])
    summa = 0
    for i, d in enumerate(CF):
        j = i + 1
        if j%2 == 0:
            summa += even[d]
        else:
            summa += odd[d]
    rem = summa % 26
    CHECK = remainder[rem]

    return " ".join([SUR, NAM, YEAR, MONTH, DAY, POB, CHECK])

    

if __name__ == "__main__":

    date = (1, 1, 1988)
    name = "Mario"
    surname = "Rossi"
    pob = "Roma"
    
    print(cf(name, surname, 'M', date, pob))
